/*
 * Icon configuration file for the GNU Hurd system
 */

#define UNIX 1
#define LoadFunc

#define CComp "gcc"
#define COpts "-O -fomit-frame-pointer"
