/*
 * Icon configuration file for Cygwin environment on Microsoft Windows
 */
#define MSWIN 1		/* this configuration is for Microsoft Windows */
#define CYGWIN 1	/* this configuration uses Cygwin API */

#define FAttrib		/* enable fattrib() extension */
#define WinExtns	/* enable native Windows functions */

#define CComp "gcc"

#define ExecSuffix ".exe"
#define IcodeSuffix ".exe"

#define BinaryHeader
#define MaxHdr 16384 
